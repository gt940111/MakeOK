import {NgModule} from "@angular/core";

import {FormsModule} from "@angular/forms";
import {KittensComponent} from "./kittens.component";
import {KittensAgeComponent} from "./kittensAge.component";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule.forChild([{
      path: "",
      component: KittensComponent
    }])
  ],
  declarations: [
    KittensComponent,
    KittensAgeComponent
  ],
  exports: [
    KittensComponent
  ],
  bootstrap: [KittensComponent]
})
export class KittensModule {}
