import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: 'kittens-age',
  template: `
    <label  for="setNewKittensAge"> Age (months):</label>
    <input class="form-control" id="setNewKittensAge" type="number" maxlength="2" style="width: 40%;" max="12" min="0"
        [(ngModel)]="age" />
  `
})
export class KittensAgeComponent {
  _age: number = 0;

  @Output() onChanged = new EventEmitter<number>();

  @Input()
  set age(age: number) {
    if (age < 0) this._age = 0;
    else if (age > 12) this._age = 12
    else this._age = age;

    this.onChanged.emit(age);
  }

  get age() { return this._age}

}
