import {Component} from '@angular/core';

class Kitten {
  name: string;
  hasCollar: boolean;
  age: number;

  constructor(name: string, hasCollar: boolean, age: number) {
    this.name = name;
    this.hasCollar = hasCollar;
    this.age = age;
  }
}


@Component({
  selector: 'kittens',
  templateUrl: './kittens.html',
  styleUrls: ['./kittens.css']
})
export class KittensComponent {
  kittenAge: number = 2;

  name: string = "";
  hasCollar: boolean = false;

  kittens: Kitten[] = [
    {name: "Simba", hasCollar: false, age: 2},
    {name: "Milo", hasCollar: true, age: 4},
    {name: "Loki", hasCollar: false, age: 1}
  ]

  addKitten(name: string, hasCollar: boolean, age: number): void {
    if (name == null || name.trim() == "") {
      return;
    }

    this.kittens.push(new Kitten(name, hasCollar, age));
  }

  onAgeChanged(age: number) {
    this.kittenAge = age;
  }

}
