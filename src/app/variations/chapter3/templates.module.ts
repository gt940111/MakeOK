import {NgModule} from "@angular/core";

import {FormsModule} from "@angular/forms";
import {ChildComponent} from "./child.component";
import {TemplatesComponent} from "./templates.component";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {Child2Component} from "./child2.component";
import {SegaDirective} from "./segamegadrive.directive";
import {Child3Component} from "./child3.component";
import {WhileDirective} from "./while.directive";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule.forChild([{
      path: "",
      component: TemplatesComponent
    }])
  ],
  declarations: [
    TemplatesComponent,
    ChildComponent,
    Child2Component,
    Child3Component,
    SegaDirective,
    WhileDirective
  ],
  exports: [
    TemplatesComponent
  ],
  bootstrap: [TemplatesComponent]
})
export class TemplatesModule {}
