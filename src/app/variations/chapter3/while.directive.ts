import {Directive, Input, TemplateRef, ViewContainerRef} from "@angular/core";

//Структурная директива должна применять декоратор Directive,
// в который передается название селектора директивы в квадратных скобках.
@Directive({ selector: '[while]'})
//аналог директивы ngIf
export class WhileDirective {

  /*
  *Для получения доступа к шаблону директивы применяется объект TemplateRef.
  * Этот объект автоматически передается в конструктор через механизм внедрения зависимостей.
  * Кроме этого объекта в конструктор также передается объект рендерера - ViewContainerRef.
  *
  * C помощью применения модификатора private для обоих этих параметров автоматически
  * будут создаваться локальные переменные, к которым затем можно обратиться.
  * */
  constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef) {}

  @Input() set while(condition: boolean) {
    if (condition) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
    else {
      this.viewContainer.clear();
    }
  }
}
