import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: 'child-app',
  styles: ['.verdana{font-size: 35px}'],
  template: `
    <div style="background: #d9d9d9;">
      <button (click)="change(true)">+</button>
      <button (click)="change(false)">-</button>
    </div>
    <br/><hr/>

    <div style="background: #d9d9d9;">
        <h4 [ngClass]="{verdana:verdanaTrue}">invoke parent's "onChange()" from child-component</h4>
        <button [ngClass]="bootstrap_classes" (click)="verdanaTrue = !verdanaTrue">change to: {{!verdanaTrue}}</button>
        <br/>
        <input [ngModel]="someName" (ngModelChange)="onNameChange($event)" />
        <p>SomeName: {{someName}}</p>
    </div>
    <br/><hr/>

    <p style="background: #d9d9d9;">Child counter: {{counter}}</p>
  `
})
export class ChildComponent {
  counter: number = 0;
  verdanaTrue: boolean = true;

  bootstrap_classes = {
    btn: true,
    rounded: true
  }

  increment() {this.counter++;}
  decrement() {this.counter--;}

  @Input() someName: string = "";

  @Output() someNameChange = new EventEmitter<string>();
  onNameChange(model: string){
    this.someName = model;
    this.someNameChange.emit(model);
  }


  @Output() onChanged = new EventEmitter<boolean>();
  change(input: boolean) {
    this.onChanged.emit(input);
  }

}
