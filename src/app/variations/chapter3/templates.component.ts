import {Component, ElementRef, ViewChild} from '@angular/core';
import {Child2Component} from "./child2.component";

@Component({
  selector: 'some-component',
  templateUrl: './template.html',
  styles: [
    '.width90{width: 100%}'
  ]
})
export class TemplatesComponent {
  clicks: number = 0;

  onChanged(input: boolean) {
    input ? this.clicks++ : this.clicks--;
  }

  userName: string = "SomeNewString";
  nameForContentChild: string = "name for @ContentChild"


  @ViewChild(Child2Component, {static: false})
  private counterComponent: Child2Component|undefined;
  increment2() { this.counterComponent?.increment2(); }
  decrement2() { this.counterComponent?.decrement2(); }

  @ViewChild("someText", {static: false})
  textField: ElementRef|undefined;

  changeTextField() {
    if (this.textField?.nativeElement.textContent === "#Template") {
      this.textField.nativeElement.textContent = "тык ";
      return;
    }
    if (this.textField !== undefined) {
      this.textField.nativeElement.textContent += "тык "
    }
  }
}
