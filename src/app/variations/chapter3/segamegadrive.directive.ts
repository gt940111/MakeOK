import {Directive, ElementRef, HostBinding, Input, Renderer2} from "@angular/core";

//Атрибутивные директивы меняют поведение элемента, к которому они применяются.
@Directive({
  selector: '[segamegadrive]',
  host: { //(2)
    '(mouseenter)': 'onMouseEnter()',
    '(mouseleave)': 'onMouseLeave()'
  }
})
export class SegaDirective{
  private fontWeight = "normal";
  private fontSize: string;

  @Input() selectedSize = "18px";
  @Input() defaultSize = "16px";

  /*
  * Класс "ElementRef" представляет ссылку на элемент, к которому будет применяться директива.
  * Renderer2 представляет сервис, который также при вызове директивы автоматически передается
  * в ее конструктор, и мы можем использовать данный сервис для стилизации элемента.
  * */
  constructor(private element: ElementRef, private renderer: Renderer2) { //(1)
    this.renderer.setStyle(element.nativeElement, "cursor", "pointer")
    this.fontSize = this.defaultSize;
  }

  @HostBinding("style.fontWeight") get getFontWeight() {
    return this.fontWeight;
  }

  @HostBinding("style.fontSize") get getFontSize() {
    return this.fontSize;
  }

  @HostBinding("style.cursor") get getCursor() {
    return "pointer";
  }

  //(2) @HostListener("mouseenter")
  onMouseEnter() {
    //(1) this.setFontWeight("bold");
    this.fontWeight = "bold";
    this.fontSize = this.selectedSize;
  }

  //(2) @HostListener("mouseleave")
  onMouseLeave() {
    //(1) this.setFontWeight("normal");
    this.fontWeight = "normal";
    this.fontSize = this.defaultSize;
  }

  /*(1)
  private setFontWeight(val: string) {
    this.renderer.setStyle(this.element.nativeElement, "font-weight", val);
  }*/

}
