import {Component, ContentChild, ElementRef} from "@angular/core";

@Component({
  selector: "child2-component",
  template: `
    <ng-content></ng-content>
    <div style="background: #d9d9d9;">
      <button (click)="changeHeader()">Изменить @ContentChild</button>
    </div>
    <hr/>
    <p style="background: #d9d9d9;">Child2 value: {{counter}}</p>

  ` //<p>Child2 someText: {{someText}}</p>
})
export class Child2Component {
  counter: number = 0;

  increment2() {this.counter++}
  decrement2() {this.counter--}


  //someText: string = "это типа текст";


  @ContentChild("headerContent", {static: false})
  header: ElementRef|undefined;

  changeHeader() {
    if (this.header !== undefined) {
      this.header.nativeElement.textContent = "Changed!"
    }
  }
}
