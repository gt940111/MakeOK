import {Component} from "@angular/core";

@Component({
  selector: 'child3',
  template: `
    <div>
      <p *ngIf="condition">ngIf is "true"</p>
      <p *ngIf="!condition">ngIf is "false"</p>

      <p *ngIf="condition;else unset">ngIf = true;else template</p>
      <ng-template #unset><p>template</p></ng-template>

      <div *ngIf="condition;then thenBlock else elseBlock">
        <p>ngIf = true;then thenBlock else elseBlock</p>
      </div>
      <ng-template #thenBlock><p>thenBlock</p></ng-template>
      <ng-template #elseBlock><p>elseBlock</p></ng-template>

      <button (click)="toggle()">Toggle</button>
    </div>

    <br/>
    <hr/>
    <p>аналог, блин :(</p>
    <div>
      <p *while="condition">параграф 1</p>
      <p *while="!condition">параграф 2</p>
      <p class="rounded-circle" style="border: solid #dc6d7f; border-radius: 8%; background: rgba(239,145,145,0.68); width: 40%; margin: 0 auto">
        кнопка
        выше ^^^</p>
    </div>

    <br/>
    <hr/>
    <div>
      <p>let abc of stringList:</p>
      <div *ngFor="let abc of stringList">{{abc}}</div>
      <br/>
      <div>
        <span *ngFor="let abc of stringList; let i = index">
          | {{stringList.length - i}}.{{stringList[stringList.length - i - 1]}} |
        </span>
      </div>
    </div>

    <br/>
    <hr/>
    <div>
      <div [ngSwitch]="count">
        <ng-template ngSwitchCase="1">Let it be 1</ng-template>
        <ng-template ngSwitchCase="2">Now 2</ng-template>
        <ng-template ngSwitchCase="3">You reached the limit</ng-template>
        <ng-template ngSwitchCase="default">it's a default</ng-template>
        <ng-template ngSwitchDefault>[empty]</ng-template>
      </div>
      <button class="btn btn-danger" (click)="increase()">+</button>
      <button class="btn btn-danger" (click)="decrease()">-</button>
    </div>
  `
})
export class Child3Component{
  condition: boolean = true;

  stringList: string[] = ['string1', 'string two', 'three'];
  length: number = this.stringList.length;

  toggle() {
    this.condition = !this.condition;
  }

  count: number = 0;
  increase() {
    if (this.count < 3) this.count++;
  }
  decrease() {
    if (this.count > 0) this.count--;
  }

}
