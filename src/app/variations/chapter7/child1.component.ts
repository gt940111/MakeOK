import {Component} from "@angular/core";
import {Observable} from "rxjs";

@Component({
  selector: 'ch7-child1',
  template: `
    <p>Child 1</p>
    <button (click)="openExit()">{{exit ? "you are free" : "LET ME OUT!"}}</button>
  `
})
export class Ch7Child1Component{
  exit: boolean = false;

  openExit() {
    this.exit = !this.exit;
  }

  /*
  Далее в классе AboutComponent реализуем метод canDeactivate(). Этот метод и будет вызываться в ExitAboutGuard.
  Поэтому в этом методе мы можем проверить состояние компонента и решить,
  стоит ли делать переход, надо ли выполнить какие-то дополнительные действия и т.д.*/
  canDeactivate(): boolean | Observable<boolean> {
    return this.exit ? true : confirm("exit?");
  }
}
/*
* Во-первых, в определении маршрута добавляется параметр
canDeactivate: [ExitAboutGuard]
*
Во-вторых, сам класс ExitAboutGuard добавляется в список провайдеров:
providers: [ AboutGuard, ExitAboutGuard],
*
И теперь при попытке ухода с компонента AboutComponent (если saved == false), мы увидим диалоговое окно для подтверждения перехода:
* */
