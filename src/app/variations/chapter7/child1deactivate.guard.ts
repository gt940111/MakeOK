import {CanDeactivate} from "@angular/router";
import {Observable} from "rxjs";

export interface ComponentCanDeactivate{
  canDeactivate: () => boolean | Observable<boolean>;
}

/*
ExitAboutGuard должен реализовать метод canDeactivate() интерфейса CanDeactivate.
Этот метод собственно и управляет уходом с компонента и переходом на другой компонент.
*/
export class ExitChild1Guard implements CanDeactivate<ComponentCanDeactivate>{

  /*
  Для управления навигацией в этот метод передается компонент, с которого осуществляется переход.
  Но передаваемый параметр должен реализовать определенный интерфейс - в данном случае ComponentCanDeactivate.

  Если нельзя осуществить переход, то возвращается значение false, иначе возвращается значение true.
  Это может быть просто логическое значение, либо же логическое значение, обернутое в объект Observable.*/
  canDeactivate(component: ComponentCanDeactivate) : Observable<boolean> | boolean{

    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
