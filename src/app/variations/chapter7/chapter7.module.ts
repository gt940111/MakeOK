import {NgModule} from "@angular/core";
import {Chapter7Component} from "./chapter7.component";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {Ch7Child1Component} from "./child1.component";
import {Ch7Child2Component} from "./child2.component";
import {Child1Guard} from "./child1.guard";
import {ExitChild1Guard} from "./child1deactivate.guard";


@NgModule({
  imports: [CommonModule, FormsModule, HttpClientModule,
    RouterModule.forChild([
      {
        path: "",
        component: Chapter7Component
      },
      {
        path: "child1",
        component: Ch7Child1Component,
        //в определении этого маршрута прописывается параметр canActivate: [...Guard].
        canActivate: [Child1Guard],
        canDeactivate: [ExitChild1Guard]
      },
      {
        path: "child2",
        component: Ch7Child2Component
      }
    ])
  ],
  declarations: [
    Chapter7Component,
    Ch7Child1Component,
    Ch7Child2Component
  ],
  //...Guard должен быть указан в списке провайдеров модуля
  providers: [Child1Guard, ExitChild1Guard],
  bootstrap: [Chapter7Component]
})
export class Chapter7Module {}
