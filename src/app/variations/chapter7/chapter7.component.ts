import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {Subscription} from "rxjs";

@Component({
  selector: 'chapter7-comp',
  template: `
    <h4>CHAPTER 7</h4>
    <hr/>
    <p>ID: {{id}}</p>
    <p>PRODUCT: {{product}}</p>
    <p>PRICE: {{price}}</p>
    <hr/>
    <button routerLink="./child1">Child1</button>
    <button routerLink="./child2">Child2</button>
    <button routerLink="../14" [queryParams]="{'price': 123, 'product': 'new name'}">CHANGE PARAMS</button>
  `
})
export class Chapter7Component {

  id: number | undefined;
  product: string | undefined;
  price: number | undefined;



  /*
   * Метод subscribe() позволяет установить подписку на изменение параметра маршрута.
   * В этом случае компонент будет получать новое значение, и проблем с навигацией по ссылкам с разными параметрами не возникнет.
   */
  //private routeSubscription: Subscription | undefined;
  private querySubscription: Subscription;

  /*
  * Для получения параметров маршрута нам необходим специальный сервис ActivatedRoute.
  * Он содержит информацию о маршруте, в частности, параметры маршрута, параметры строки запроса и прочее.
  *
  * Свойство snapshot хранит состояние маршрута, а состояние маршрута содержит переданные параметры.
  * */
  constructor(private activateRoute: ActivatedRoute) {
    //this.id = activateRoute.snapshot.params['id'];
    //this.routeSubscription = activateRoute.params.subscribe(params => this.id = params['id']);

    /*Получение параметров из строки запроса аналогично получению данных из маршрута,
     только в данном случае применяется свойство queryParams класса ActivatedRoute.*/
    this.querySubscription = activateRoute.queryParams.subscribe(
      (queryParam: any) => {
        this.product = queryParam['product'];
        this.price = +queryParam['price'];
      }
    )

    /*
    * И как и в случае с параметрами маршрута, результатом вызова route.queryParams.subscribe() является объект Subscription,
    * который необходимо удалять при удалении компонента в методе ngOnDestroy.
    */
  }

  /*
  В данном случае получение id происходит в методе ngOnInit.
  Для этого происходит обращение к свойству this.route.paramMap, которое представляет объект Observable<ParamMap>
  (ParamMap представляет карту параметров - как параметров маршрута, так и строки запроса).
  Далее у Observable вызывается метод pipe(), который позволяет создать цепочку операторов rxjs.

  В методе pipe() вызывается оператор switchMap(), который в качестве параметра принимает функцию с одним параметром - ParamMap.
  Таким образом мы извлекаем карту параметров и берем из нее значение параметра id.
  При изменении значения параметра switchMap позволяет получить новое значение.

  По умолчанию значение параметра id представляет строку,
  поэтому далее с помощью метода subcribe мы подписываемся на объект Observable и приналичии данных извлекаем значение id.
  (Плюс перед значением +data указывает на преобразование к числу).
  * */
  ngOnInit() {
    this.activateRoute.paramMap
      .pipe(
        switchMap(params => params.getAll('id'))
      )
      .subscribe(data => this.id = +data);
  }

}
