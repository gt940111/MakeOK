import {Component} from "@angular/core";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

//Но чтобы все это заработало, необходимо импортировать модуль ReactiveFormsModule в (здесь)chapter5.module.ts
@Component({
  selector: "reactive-c5",
  styles: [`
        input.ng-touched.ng-invalid {border:solid red 2px;}
        input.ng-touched.ng-valid {border:solid green 2px;}
    `],
  template: `
    <form [formGroup]="myForm">
      <div class="form-group">
        <label>Name</label>
        <input class="form-control" name="name" formControlName="userName"/>
        <div class="alert alert-danger"
             *ngIf="myForm.controls['userName'].invalid && myForm.controls['userName'].touched">
          Не указано имя
        </div>
      </div>
      <div class="form-group">
        <label>Email</label>
        <input class="form-control" name="email" formControlName="userEmail"/>
        <!--С помощью выражений myForm.controls['userName'] мы можем обратиться к нужному элементу формы и получить его состояние или значение. -->
        <div class="alert alert-danger" *ngIf="myForm.controls['userEmail'].invalid && myForm.controls['userEmail'].touched">
          <p *ngIf="myForm.controls['userEmail'].value == ''; else elseEmail">Где мыло?</p>
          <ng-template #elseEmail>Некорректное мыло</ng-template>
        </div>
      </div>
      <div class="form-group">
        <label>Phone</label>
        <div formArrayName="userPhone">
          <div *ngFor="let phone of getFormsControls()['controls']; let i = index">
            <input class="form-control" formControlName="{{i}}"/>
          </div>
        </div>
        <button class="btn btn-default" (click)="addPhone()">
          +
        </button>
      </div>
      <div class="form-group">
        <input class="form-control btn btn-dark" type="submit" [disabled]="myForm.invalid" (click)="onSubmit()"/>
        <label style="color: green">{{text}}</label>
      </div>
    </form>
    <br/>
  `
})
export class ReactiveFormsComponent {
  text: string = "";
  phonePattern: string = "\\+(\\d((\\s|-)?\\d{3}){2}((\\s|-)?\\d{2}){2})";

  myForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    /*this.myForm = new FormGroup({
      "userName": new FormControl("null", [Validators.required, this.userNameValidator]),
      "userEmail": new FormControl("", [Validators.required, Validators.email]),
      "userPhone": new FormArray([
        new FormControl("+7", Validators.pattern(this.phonePattern))
      ])
    })*/

    //Класс FormBuilder представляет альтернативный подход к созданию форм
    //FormBuilder передается в качестве сервиса в конструктор.
    // - С помощью метода group() создается объект FormGroup. Каждый элемент передается в форму в виде обычного массива значений:
    this.myForm = formBuilder.group({
      //в качестве первого параметра можно передавать значение по умолчанию для элемента,
      //а в качестве второго параметра - набор валидаторов:
      "userName": ["null", [Validators.required]],
      "userEmail": ["", [Validators.required]],
      //поля для ввода телефонных номеров представляют массив. Массив или FormArray хранит набор объектов FormControl.
      "userPhone": formBuilder.array([
        ["+7", Validators.pattern(this.phonePattern)]
      ])
    })

  }


  /*
  Для упрощения получения массива элементов ввода определен метод getFormsControls(), который возвращает объект FormArray.
  В коде html предусматриваем вывод объектов из FormArray, возвращаемого методом getFormsControls(), на форму с помощью директивы "ngFor";
  При этом контейнер всех элементов ввода имеет директиву formArrayName="phones",
  а каждый элемент в качестве названия принимает его текущий индекс: formControlName="{{i}}".*/
  getFormsControls(): FormArray {
    return this.myForm.controls['userPhone'] as FormArray;
  }


  //Чтобы можно было динамически при необходимости добавлять новые объекты, в классе компонента предусмотрен метод addPhone():
  addPhone() {
    /*В этой сложной конструкции мы сначала получаем объект формы через выражение this.myForm.controls["phones"],
    затем приводим его к типу FormArray.
    И далее, как и в обычный массив, добавляем через метод push новый элемент.*/
    (<FormArray>this.myForm.controls['userPhone'])
      .push(new FormControl("+7", Validators.pattern(this.phonePattern)))
  }

  onSubmit() {
    console.log(this.myForm);
    this.text = "Check console log";
  }


  //Кроме использования встроенных валидаторов мы также можем определять свои валидаторы.

  //В качестве параметра он принимает элемент формы, к которому этот валидатор применяется,
  // а на выходе возвращает объект, где ключ - строка, а значение равно true.
  userNameValidator(control: FormControl):{[s:string]: boolean}|null {

    /*В данном случае проверяем, если значение равно строке "нет", то возвращаем объект {"userName": true}.
    Значение true указывает, что элемент формы не прошел валидацию. Если же все нормально, то возвращаем null.*/
    if(control.value==="нет"){
      return {"userName": true};
    }
    return null;
  }
  /*Затем этот валидатор добавляется к элементу userName (здесь - в конструкторе): this.userNameValidator
  * И в случае если в поле для ввода имени будет введено значение "нет", то данное поле не пройдет валидацию:*/

}
