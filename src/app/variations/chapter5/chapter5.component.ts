import {Component} from "@angular/core";

@Component({
  selector: "services-component",
  template: `
    <div class="col-sm-4">
      <h2 class="text-center">CHILD 1</h2>
      <hr/>
      <child-s-c></child-s-c>
    </div>
    <div class="col-sm-4">
      <h2 class="text-center">CHILD 2</h2>
      <hr/>
      <child2-s-c></child2-s-c>
    </div>
    <div class="col-sm-4">
      <h2 class="text-center">Reactive Forms</h2>
      <hr/>
      <reactive-c5></reactive-c5>
    </div>
  `
})
export class Chapter5Component{ }
