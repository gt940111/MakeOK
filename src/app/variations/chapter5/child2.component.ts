import {Component} from "@angular/core";
import {NgForm} from "@angular/forms";


@Component({
  selector: "child2-s-c",
  template: `
    <!--
    * Данная директива инкапсулирует все элементы формы, к которым применяется директива ngModel,
    и позволяет получить доступ к состоянию этих элементов, в том числе проверить введенные данные на корректность.
    * Для отслеживания состояния Angular добавляет к тегу форму ряд классов: class="ng-pristine ng-untouched ng-valid"

    * "novalidate" отключает отображение встроенных в браузер сообщений об ошибках валидациию.
    -->
    <form #myForm="ngForm" novalidate>
      <div class="form-group">
        <label>Name</label>
        <input class="form-control" name="name" [(ngModel)]="name" required/>
      </div>
      <div class="form-group">
        <label>Email</label>
        <!-- "ngModel" просто указывает, что поле ввода, к которому она применяется, будет включаться в объект myForm. -->
        <input class="form-control" name="email" ngModel required email/>
      </div>
      <div class="form-group">
        <label>Phone</label>
        <input class="form-control" name="phone" ngModel pattern="[0-9]{10}"/>
      </div>
      <div class="form-group">
        <!-- используя состояние формы, мы можем отключить кнопку: -->
        <!--<button [disabled]="myForm.invalid" class="btn btn-toolbar" (click)="submit(myForm)">CLICK</button>-->
        <input type="submit" [disabled]="myForm.invalid" class="btn btn-toolbar" value="Send" /> <!--onSubmit()-->
      </div>
    </form>
    <!-- доступ к соответствующему элементу возможен, только если к этому элементу применяется директива ngModel -->
    <div>Имя: {{myForm.value.name}}</div>
    <div>Email: {{myForm.value.email}}</div>
  `,
  /*используя классы ng-valid и ng-invalid, мы можем задать дополнительные возможности по стилизации.*/
  styles: [`
        input.ng-touched.ng-invalid {border:solid red 2px;}
        input.ng-touched.ng-valid {border:solid green 2px;}
    `],
})
export class Child2SComponent {

  name: string = "";
  email: string = "";
  phone: string = "";

  /*myForm представляет сложный объект, который инкапсулирует всю информацию о форме*/
  submit(form: NgForm) {
    console.log(form);
  }
  onSubmit(form: NgForm) {
    console.log(form);
  }

}
