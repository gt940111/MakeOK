import {NgModule} from "@angular/core";
import {Chapter5Component} from "./chapter5.component";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {ChildSComponent} from "./child.component";
import {Child2SComponent} from "./child2.component";
import {ReactiveFormsComponent} from "./ReactiveForms.component";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule.forChild([{
      path: "",
      component: Chapter5Component
    }]), ReactiveFormsModule],
  declarations: [Chapter5Component, ChildSComponent, Child2SComponent, ReactiveFormsComponent],
  bootstrap: [Chapter5Component]
})
export class Chapter5Module { }
