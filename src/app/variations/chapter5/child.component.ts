import {Component} from "@angular/core";
import {NgModel} from "@angular/forms";

export class Phone{
  constructor(public title: string,
              public price: number,
              public company: string)
  { }
}
/*
Если элемент ввода еще не получал фокус, то устанавливается класс ng-untouched (ng-touched)
Если же значение не изменялось с момента загрузки страницы, то к элементу ввода применяется класс ng-pristine (ng-dirty)
Если значение в поле ввода корректно, то применяется класс ng-valid (ng-invalid)
* */
@Component({
  selector: 'child-s-c',
  template: `
    <div class="row">
      <div class="col-md-3">
        <label>Модель</label>
        <input class="form-control" type="text" name="title" [(ngModel)]="phone.title" #phoneTitle="ngModel" (ngModelChange)="onTitleChange()" required
        pattern="\\w{3}(\\w|\\s)*"/>
        <!--required: требует обязательного ввода значения
          pattern: задает регулярное выражение, которому должны соответствовать вводимые данные-->
        <!--<div [hidden]="phoneTitle.untouched || phoneTitle.valid" class="alert alert-danger"> &lt;!&ndash;style="color: red"&ndash;&gt;
          Name not specified
        </div>-->
      </div>
      <div class="col-md-3">
        <label>Цена</label>
        <input class="form-control" type="number" name="price" [(ngModel)]="phone.price" #phonePrice="ngModel" (change)="onPriceChange()"/>
      </div>
      <div class="col-md-3">
        <label>Производитель</label>
        <select class="form-control" name="company" [(ngModel)]="phone.company" #phoneCompany="ngModel">
          <option *ngFor="let comp of companies" [value]="comp">{{comp}}</option>
        </select>
      </div>
      <div class="col-md-3">
        <br/>
        <button class="btn btn-default" (click)="addPhone(phoneTitle, phonePrice, phoneCompany)"
        [disabled]="phoneTitle.invalid">Add Phone</button>
        <!--С помощью выражения [disabled]="name.invalid || email.invalid || phone.invalid" для атрибута disabled
        устанавливается значение true, то есть кнопка отключается, если хотя бы одно из полей не валидно.-->
      </div>

    </div>
    <hr/>
    <table>
      <thead>
      <tr>
        <th class="col-sm-4">Company</th>
        <th class="col-sm-4">Phone</th>
        <th class="col-sm-4">Price</th>
      </tr>
      </thead>
      <tbody>
      <tr *ngFor="let tel of phoneList">
        <td class="col-sm-4">{{tel.company}}</td>
        <td class="col-sm-4">{{tel.title}}</td>
        <td class="col-sm-4">{{tel.price}}</td>
      </tr>
      </tbody>
    </table>
  `,
  /*используя классы ng-valid и ng-invalid, мы можем задать дополнительные возможности по стилизации.*/
  styles: [`
        input.ng-touched.ng-invalid {border:solid red 2px;}
        input.ng-touched.ng-valid {border:solid green 2px;}
    `],
})
export class ChildSComponent{
  phone: Phone = new Phone("", 0, "");
  companies: string[] = ["Apple", "Huawei", "Xiaomi", "Samsung", "Nokia", "ZTE", "Other"];

  phoneList: Phone[] = [];
  addPhone(title:NgModel, price: NgModel, comp: NgModel) {
    this.phoneList.push(new Phone(title.model, price.model, comp.model));
    console.log("this.phone: " + this.phone);
  }

  onTitleChange() { //событие срабатывает динамически   (ngModelChange)="onTitleChange()"
    switch (this.phone.title) {
      case "111":   this.phone.title = "ZTE Axon 10";         this.phone.company = "ZTE";     break;
      case "222":   this.phone.title = "Samsung Galaxy S20";  this.phone.company = "Samsung"; break;
      case "333":   this.phone.title = "Xiaomi Mi 11";        this.phone.company = "Xiaomi";  break;
      case "444":   this.phone.title = "Huawei Mate 40 Pro";  this.phone.company = "Huawei";  break;
      case "555":   this.phone.title = "Nokia 8.3 5G";        this.phone.company = "Nokia";   break;
      case "66":    this.phone.title  = "Apple iPhone 11";    this.phone.company = "Apple";   break;
      case "777":   this.phone.title = "OnePlus 9";           this.phone.company = "Other";   break;
      case "888":   this.phone.title = "Infinix Note 10 Pro"; this.phone.company = "Other";   break;
      case "999":   this.phone.title = "Asus Zenfone 8";      this.phone.company = "Other";   break;
      case "000":   this.phone.title = "Honor 30 Pro Plus";   this.phone.company = "Other";   break;
      default:
        this.phone.price = 250;
        break;
    }
  }
  onPriceChange() { //событие срабатывает, когда мы покинем данное поле ввода.  (change)="onPriceChange()"
    if (this.phone.price == 100) {
      this.phone.price = 150;
    } //fehlende Kenntnisse
  }

}
