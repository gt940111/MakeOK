import {NgModule} from "@angular/core";
import {Chapter6Component} from "./chapter6.component";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {UserHttpComponent} from "./user.http.component";

@NgModule({
  imports: [CommonModule, FormsModule, HttpClientModule,
    RouterModule.forChild([{
      path: "",
      component: Chapter6Component
    }])
  ],
  declarations: [
    Chapter6Component,
    UserHttpComponent
  ],
  bootstrap: [Chapter6Component]
})
export class Chapter6Module {}
