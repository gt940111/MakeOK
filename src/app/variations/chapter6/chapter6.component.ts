import {Component} from "@angular/core";

@Component({
  selector: 'chapter6-component',
  template: `
    <div>Chapter 6</div>
    <div>
      <chapter6-user></chapter6-user>
    </div>
  `
})
export class Chapter6Component {

}
