import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {catchError, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {User} from "./user";
import {Todo} from "./todo";

/*
При взаимодействии с сервером, как правило, обращения к серверу происходят не непосредственно из компонента, а из вспомогательных сервисов.
Поскольку сервис может определять дополнительную логику обработки полученных с сервера данных, которую могли бы сделать код компонента перегруженным.
Кроме того, сервисы могут определять функционал, который будет использоваться несколькими компонентами.
*/
@Injectable()
export class HttpService {
  errorMessage: string = "";

  constructor(private http: HttpClient) {}

  getDataFromJson(file: string) {
    return this.http.get(file);
  }

  /*У результата метода get() мы можем вызвать метод pipe(), который позволяет обработать результаты запроса.
  Для этого метод pipe в качестве первого параметра принимает функцию обработки данных запроса.
  В данном случае в роли такой функции выступает оператор map, который преобразует результаты запроса в новые объекты.*/
  getUsersArray(): Observable<User[]> {
    return this.getDataFromJson('assets/chapter6/users.json')
      //.pipe(map(), catchError())
       .pipe(
         map((data:any) => { //map to User[]
            let usersList = data["userList"];
            return usersList.map(function(user: any): User { //map each object to User
              return new User(user.name, user.age);
            }); //В итоге весь метод getUsers() возвращает объект Observable<User[]>.
         }),

         //Для перехвата ошибок, которые могут возникнуть при выполнении запроса, можно использовать функцию catchError().
         catchError(err => {
           console.log(err);
           this.errorMessage = err.message;
           return [];
         })
       )
  }

  getTodosFromExternal() {
    return this.http.get('https://jsonplaceholder.typicode.com/todos')
      .pipe(
        map((data:any) => {
          return data.map((todo: any) => new Todo(todo.userId, todo.id, todo.title, todo.completed));
        }),
        catchError(err => {
          console.log(err);
          this.errorMessage = err.message;
          return [];
        })
      );
    /*return this.http.get('http://localhost:3000/todos')
      .pipe(
        map((data:any) => {
          return data.map((todo: any) => new Todo(0, todo.id, todo.title, todo.completed));
        }),
        catchError(err => {
          console.log(err);
          this.errorMessage = err.message;
          return [];
        })
      );*/
  }

  //this.http.get('http://localhost:3000/sum?num1=' + num1 + "&num2=" + num2);
  /*
  С помощью метода set() объекта HttpParams устанавливаются параметры, и затем этот объект передается в запрос.
  В итоге результат будет тот же, что и в примере выше.

  const params = new HttpParams().set('num1', num1.toString()).set('num2', num2.toString());
  return this.http.get('http://localhost:3000/sum', {params});*/

  putTodoCompleted(todo: Todo) {
    todo.completed = !todo.completed;
    return this.http
      .put(
        'https://jsonplaceholder.typicode.com/todos/' + todo.id,
        JSON.stringify(todo),
        {headers: {'Content-type': 'application/json; charset=UTF-8'}}
      );
    //return this.http.put<any>('http://localhost:3000/complete?id='+todo.id, null);
  }

}


/*
При отправке запроса из другого домена опять же следует не забывать
про ограничения кроссдоменных запросов. Поэтому для тестирования вне зависимости от технологии сервера,
для серверного приложения должна быть включена функциональность CORS.
Иначе наше приложение на Angular не сможет получить результат обработки.
*/
