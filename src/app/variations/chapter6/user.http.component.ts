import {Component} from "@angular/core";
import {User} from "./services/user";
import {HttpService} from "./services/http.service";
import {Todo} from "./services/todo";

@Component({
  selector: 'chapter6-user',
  template: `
    <div class="col-sm-6">
      <h4>Load from files</h4>
      <hr/>
      <div>
        <p>Name: {{user?.name}}</p>
        <p>Age: {{user?.age}}</p>
      </div>
      <hr/>
      <ul>
        <li *ngFor="let someUser of users">{{someUser?.name}}, {{someUser?.age}} y.o.</li>
        <p>{{this.http.errorMessage}}</p>
      </ul>
    </div>


    <div class="col-sm-6">
      <h4>REST Calls</h4>
      <hr/>
      <div>
        <button (click)="loadTodosFromExternal()">GET(todos from external source)</button>
        <p>Number of todos: {{todos.length}}</p>

        <div *ngIf="todos.length > 0">
          <table class="table table-striped">
            <tbody>
            <tr *ngFor="let todo of todos.slice(todoIndex, todoIndex+10 > todos.length ? todos.length : todoIndex+10); let i = index">

              <td> [{{todos[todoIndex+i]?.id}}]</td>
              <td>{{todos[todoIndex+i]?.title}}</td>

              <td>
                <button *ngIf="!todos[todoIndex+i]?.completed; else elseButton" class="btn-warning"
                        (click)="completeTodo(todoIndex+i)">
                  PUT(true)
                </button>
                <ng-template #elseButton>
                  <button class="btn-success" (click)="completeTodo(todoIndex+i)">PUT(false)</button>
                </ng-template>
              </td>
            </tr>
            </tbody>
          </table>

          <button class="btn btn-dark" (click)="decreaseTodoIndex()" [disabled]="todoIndex == 0">{{todoIndex}}</button>
          ...
          <button class="btn btn-dark" (click)="increaseTodoIndex()" [disabled]="todoIndex >= todos.length - 11">{{todoIndex+11}}</button>

        </div>
      </div>
    </div>
  `,
  providers: [HttpService]
})
export class UserHttpComponent {
  baseFilePath: string = 'assets/chapter6/';

  user: User | undefined;
  users: User[] = [];

  /*Загрузка данных в конструкторе компонента не очень желательна.
  В конструкторе мы просто получаем сервис HttpClient.*/
  constructor(public http: HttpService) {}

  ngOnInit() {
    /*
    * Сам метод http.get() возвращает объект Observable<Object>.
    Observable представляет своего рода поток, и для прослушивания событий из этого потока применяется метод subscribe.
    Этот метод определяет действие над результатом запроса - полученными с сервера данными.
    В данном случае действие определено в виде лямбды и передаёт данные из файла json в конструктор класса User:
    */
    this.http.getDataFromJson(this.baseFilePath+'user.json')
             .subscribe((data: any) => this.user = new User(data.name, data.age));
    /*Напрямую данные из users.json не соответствуют массиву. Массив в файле определен по ключу "userList".
    Поэтому, используя данный ключ, мы достаем нужные данные из ответа сервера: data["userList"].*/
    this.http.getUsersArray()
             .subscribe((data: User[]) => this.users = data);
  }

  todos: Todo[] = [];
  todoIndex: number = 0;
  loadTodosFromExternal() {
    this.http.getTodosFromExternal()
             .subscribe((data: Todo[]) => this.todos = data);
  }

  increaseTodoIndex() { if (this.todoIndex+10 < this.todos.length) this.todoIndex = this.todoIndex+10; }
  decreaseTodoIndex() { if (this.todoIndex-10 >= 0) this.todoIndex=this.todoIndex - 10; }

  completeTodo(index: number) {
    this.http.putTodoCompleted(this.todos[index]).subscribe((data: any) => {
      console.log(data);
      this.todos[index].completed = data.completed;
    });
  }
}

//Поскольку файл json представляет вспомогательный файл, то нам надо указать это angular cli в файле angular.json с помощью параметра "assets": ["src/assets"]:


/*Методы класса HttpClient после выполнения запроса возвращают объект Observable<any>,
который определен в библиотеке RxJS ("Reactive Extensions").
Она не является непосредственно частью Angular, однако широко используется особенно при взаимодействии с сервером по http.
Эта библиотека реализует паттерн "асинхронный наблюдатель" (asynchronous observable).
Так, выполнение запроса к серверу с помощью класса HttpClient выполняются в асинхронном режиме.*/
