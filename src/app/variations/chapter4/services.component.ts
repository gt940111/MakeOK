import {Component} from "@angular/core";
import {DataService} from "./data.service";


@Component({
  selector: "services-component",
  template: `
  <div>
    <h2 class="text-center">SERVICES</h2>
    <hr/>
    <input [(ngModel)]="name" placeholder="заметка" />
    <button class="btn btn-light" (click)="addItem(name)">Click!</button>
    <hr/>
    <table>
      <tr *ngFor="let item of items">
        <td>{{item.text}}</td>
        <td>
          <button *ngIf="!item.done" class="btn-warning" (click)="item.done = !item.done">O</button>
          <button *ngIf="item.done" class="btn-success" (click)="item.done = !item.done">V</button>
        </td>
      </tr>
    </table>
  </div>
  `,
  /*providers: [DataService, LogService]*/
  //необходимо добавить DataService в коллекцию providers компонента
  //LogService также надо зарегистрировать в списке провайдеров

  /*
  * Возможно, потребуется, чтобы компоненты использовали один и тот же объект сервиса,
  * вместо создания разных сервисов для каждого компонента.
  * Для этого мы можем зарегистрировать все сервисы не в компоненте,
  * а в главном модуле приложения AppModule:
  * */
})
export class ServicesComponent{
  items: any = [];
  name: string = "";

  constructor(private dataService: DataService) { }

  addItem(name: string) {
    if (name !== undefined && name.trim() != '') {
      this.dataService.addData(name);
    }
  }
  ngOnInit() {
    this.items = this.dataService.getData();
  }


}
