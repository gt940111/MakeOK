import {NgModule} from "@angular/core";
import {ServicesComponent} from "./services.component";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule.forChild([{
      path: "",
      component: ServicesComponent
   }])],
  declarations: [ServicesComponent],
  bootstrap: [ServicesComponent]
})
export class ServicesModule { }
