import {Injectable, Optional} from "@angular/core";
import {LogService} from "./log.service";

class TempNotes {
  text: string;
  done: boolean;

  constructor(text: string) {
    this.text = text;
    this.done = false;
  }
}

/*
Данный декоратор гарантирует, что встроенный механизм внедрения зависимостей сможет создать объект этого класса
и передать его в качестве зависимости в другой объект (в другой сервис или компонент).
В практическом плане данный декоратор необходим, если внедряемый сервис сам имеет некоторые зависимости,
как в примере с сервисом DataService.
 */
@Injectable({providedIn: 'root'}) /*{providedIn: DataModule}*/
export class DataService {

  //Optional определяет сервис как опциональный и приложение не покажет ошибку, если сервис не определен как провайдер
  constructor(@Optional() private logger: LogService) { }

  private data: TempNotes[] = [
    {text:"Do Java", done:true},
    {text:"Learn React", done:true},
    {text:"Try Angular", done:false},
  ];

  getData(): TempNotes[] {
    if (this.logger) this.logger.write("получение данных", "");
    return this.data;
  }

  addData(name: string) {
    if (this.logger) this.logger.write('добавление в список строки', name);
    this.data.push(new TempNotes(name));
  }

}

/*
* Стандартные задачи сервисов:

- Предоставление данных приложению. Сервис может сам хранить данные в памяти, либо для получения данных может обращаться к какому-нибудь источнику данных, например, к серверу.
- Сервис может представлять канал взаимодействия между отдельными компонентами приложения
- Сервис может инкапсулировать бизнес-логику, различные вычислительные задачи, задачи по логгированию, которые лучше выносить из компонентов.
Кроме того, тем самым мы также можем решить проблему повторения кода
* */
