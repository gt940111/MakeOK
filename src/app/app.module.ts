import {Routes, RouterModule} from "@angular/router";

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {KittensModule} from "./variations/kittens/kittens.module";
import {TemplatesModule} from "./variations/chapter3/templates.module";
import {LogService} from "./variations/chapter4/log.service";
import {HttpService} from "./variations/chapter6/services/http.service";
import {Chapter7Module} from "./variations/chapter7/chapter7.module";

const appRoutes: Routes = [
  { path: '', loadChildren: () => KittensModule },
  { path: 'template', loadChildren: () => import('./variations/chapter3/templates.module').then(m => m.TemplatesModule) },
  { path: 'service', loadChildren: () => import('./variations/chapter4/services.module').then(m => m.ServicesModule) },
  { path: 'forms', loadChildren: () => import('./variations/chapter5/chapter5.module').then(m => m.Chapter5Module) },
  { path: 'http', loadChildren: () => import('./variations/chapter6/chapter6.module').then(m => m.Chapter6Module) },
  { path: 'chapter7/:id', loadChildren: () => Chapter7Module }, //Токен :id представляет параметр маршрута
  { path: '**', redirectTo: '/'}
]


@NgModule({                 //Каждый модуль должен определяться с декоратором @NgModule. NgModule представляет функцию-декоратора, принимающую объект, свойства которого описывают метаданные модуля
  //компоненты, отвечают на вопрос "что?" - применить кусок кода, вынесенный в отдельный компонент, который покажет "что?"
  declarations: [           // классы представлений, которые принадлежат модулю (components, directives, pipes)
    AppComponent
  ],
  //модули, отвечают на вопрос "как?" - как и с помочью каких imports и declarations вывести на экран что-то и какой из этих declaration будет считаться важнейшим (bootstrap)
  imports: [                //другие модули, классы которых необходимы для шаблонов компонентов из текущего модуля
    BrowserModule,
    RouterModule.forRoot(appRoutes, {useHash: true}),
    KittensModule,
    TemplatesModule
  ],
  //exports: [],            //набор классов представлений, которые должны использоваться в шаблонах компонентов из других модулей
  providers: [/*DataService,*/ LogService, HttpService],            //классы, создающие сервисы, используемые модулем
  bootstrap: [AppComponent] //корневой компонент, который вызывается по умолчанию при загрузке приложения | основной компонент загрузки модуля
})
export class AppModule { }

/*
* Маршруты могут определять параметры, через которые мы можем передавать компоненту какие-то данные извне.
*
* */
