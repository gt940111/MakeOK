import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule} from "./app/app.module";
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

//platformBrowserDynamic запускает импортированный во второй строке модуль AppModule.
// После этого начинает работать вся логика, которая заложена в модуле AppModule
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
